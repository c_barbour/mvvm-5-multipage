using Cirrious.MvvmCross.ViewModels;

namespace N05MultiPage.Core.ViewModels
{
    public class FirstViewModel 
		: MvxViewModel
    {
		private string _hello = "Hello MvvmCross";
        public string Hello
		{ 
			get { return _hello; }
			set { _hello = value; RaisePropertyChanged(() => Hello); }
		}

		private MvxCommand _myCommand;
		public MvxCommand MyCommand
		{
			get
			{
				_myCommand = _myCommand ?? new MvxCommand(DoMyCommand);
				return _myCommand;
			}
		}

		private void DoMyCommand()
		{
			Hello = Hello + " World";
		}
    }
}
